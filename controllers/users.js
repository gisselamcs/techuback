var express = require('express');
var user_file = require('../user.json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.port || 3009; //puerto, sino, 3009
//const URL_BASE = '/techu-peru/v1/'
const global = require('../global');

//Peticiòn GET users
// app.get(global.URL_BASE + 'users',
//  function (request, response) {
//    //response.send({"msg":"Operaciòn GET exitosa"});
//    // el response.status(201) es para cambiar el còdigo del msj por si no se encontrara el recurso solicitado,
//    // x defecto es el còdigo 404 "Not Found"
//    response.status(201).send(user_file);
// });

//Peticiòn GET users con ID
function getUserByID (req, res) {
//app.get(global.URL_BASE + 'users/:id',
  let pos = req.params.id - 1; //let es un tipo de variable LOCAL
  console.log("GET con id = " + req.params.id);
  let respuesta = (user_file[pos] == undefined) ? {"msg":"Usuario no encontrado"} : user_file[pos];
  res.send(respuesta);
}

module.exports.getUserByID = getUserByID;
