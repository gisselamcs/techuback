var express = require('express');
var user_file = require('./user.json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.port || 3009; //puerto, sino, 3009
//const URL_BASE = '/techu-peru/v1/'
const global = require('./global');
const controller = require('./controllers/users.js');

app.use(bodyParser.json());

//Peticiòn GET users
// app.get(global.URL_BASE + 'users',
//  function (request, response) {
//    //response.send({"msg":"Operaciòn GET exitosa"});
//    // el response.status(201) es para cambiar el còdigo del msj por si no se encontrara el recurso solicitado,
//    // x defecto es el còdigo 404 "Not Found"
//    response.status(201).send(user_file);
// });

//Peticiòn GET users con ID
app.get(global.URL_BASE + 'users/:id',controller.getUserByID);

//Peticiòn GET users con Query String
// app.get(global.URL_BASE + 'users',
//   function (req, res) {
//    console.log('GET con Query String');
//    console.log(req.query.id);
//    console.log(req.query.name);
// });

//POST users
// app.post(global.URL_BASE + 'users',
//   function (req, res) {
//     console.log('POST de users');
//     // console.log('Nuevo usuario:' + req.body);
//     // console.log('Nuevo usuario:' + req.body.first_name);
//     // console.log('Nuevo usuario:' + req.body.email);
//     let newID = user_file.length + 1
//     let newUser = {
//     "id" : newID,
//     "first_name" : req.body.first_name,
//     "last_name" : req.body.last_name,
//     "email" : req.body.email,
//     "password" : req.body.password
//   }
//   user_file.push(newUser);
//   console.log("Nuevo usuario: " + newUser);
//   res.send(newUser);
//   //res.send({"msg":"POST exitoso"})
// });

//PUT users
app.put(global.URL_BASE + 'users/:id',
  function (req, res) {
    let editUser = {
    "id" : req.params.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : req.body.password
  }
  user_file[req.params.id- 1] = editUser
  // console.log("Ediciòn usuario: " + editUser);
  res.send(editUser);
});

//DELETE users
app.delete(global.URL_BASE + 'users/:id',
  function (req, res) {
    let pos = req.params.id;
    user_file.splice(pos - 1, 1);
    res.send({"msg":"Usuario eliminado"});
});

// LOGIN users
app.post(global.URL_BASE + 'users',
  function (req, res) {
    console.log('Login');
    console.log(req.body.email);
    console.log(req.body.password);
    let pos = 0;
    let flag_exits = false;
    let idUsuario = 0;
    while (pos < user_file.length && !flag_exits) {
      if (user_file[pos].email == req.body.email && user_file[pos].password == req.body.password){
        idUsuario = user_file[pos].ID
        user_file[pos].logged = true;
        flag_exits = true;
      }
      pos++;
    }
    if (flag_exits) { //fue encontrado
      writeUserDataToFile(user_file[pos-1]);
      res.send({"Encontrado":"Sì","id":user_file[pos-1]})
    } else
      res.send({"Encontrado":"No"})
});

//LOGOUT users
app.post(global.URL_BASE + 'users/:id',
  function (req, res) {
    let paramID = req.params.id;
    if (paramID == 'logued'){
      let pos = 0;
      let flag_exits = false;
      let respuesta = {
                        table: []
                      };
      while (pos < user_file.length) {
        if (user_file[pos].logged){
          respuesta.table.push(user_file[pos]);
        }
        pos++;
      }
      var json = JSON.stringify(respuesta);
      res.send(respuesta);

    } else {
      let EstuvoLogueado = false;
      let pos = 0;
      let flag_exits = false;
      let idUsuario = 0;
      while (pos < user_file.length && !flag_exits) {
        if (user_file[pos].ID == paramID) {
          if (user_file[pos].logged)  {
            user_file[pos].logged = false;
            EstuvoLogueado = true;
          }
          idUsuario = user_file[pos].ID
          flag_exits = true;
        }
        pos++;
      }
      if (flag_exits){ //fue encontrado
        console.log('Fue encontrado');
        if (EstuvoLogueado){ //estuvo logueado
          console.log('Estuvo logueado');
          res.send({"Usuario deslogueado":"Si","id":user_file[pos-1]})
        } else
            res.send({"Usuario logueado previamente":"No","id":user_file[pos-1]})
      } else
          res.send({"Usuario encontrado":"No"})
    }
});


function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./userslogued.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

app.listen(port, function () {
  console.log('Example app listening on port 3009!');
});
//El mètodo listen puede omitir la callback: app.listen(3000); con ello,
//solamente ya no se mostrarà el msj en la consola
